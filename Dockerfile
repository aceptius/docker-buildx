FROM docker:latest

RUN apk add curl jq

RUN mkdir -vp ~/.docker/cli-plugins
RUN sh -c 'RELEASE=$(curl -s https://api.github.com/repos/docker/buildx/releases/latest | jq .name -r) && \
    echo "https://github.com/docker/buildx/releases/download/${RELEASE}/buildx-${RELEASE}.linux-amd64" && \
    curl -s -L https://github.com/docker/buildx/releases/download/${RELEASE}/buildx-${RELEASE}.linux-amd64 -o ~/.docker/cli-plugins/docker-buildx'
RUN chmod -v 755 ~/.docker/cli-plugins/docker-buildx
RUN docker buildx version
